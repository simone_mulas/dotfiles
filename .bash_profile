# Only run on macOS
if [[ "$OSTYPE" == "darwin"* ]]; then
  # needed for brew
  eval "$(/opt/homebrew/bin/brew shellenv)"
fi

# Only run these on Ubuntu
if [ -d "/etc/os-release" ]; then
  if [[ $(grep -E "^(ID|NAME)=" /etc/os-release | grep -q "ubuntu")$? == 0 ]]; then
      # needed for brew to work
      eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
  fi
fi
if [ -r ~/.bashrc ]; then
  source ~/.bashrc
fi


