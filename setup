#!/bin/bash

set -xe

# create directories
export XDG_CONFIG_HOME="$HOME"/.config
mkdir -p "$XDG_CONFIG_HOME"/bash
mkdir -p "$XDG_CONFIG_HOME"/alacritty

# set up git prompt
# curl -L https://raw.githubusercontent.com/git/git/master/contrib/completion/git-prompt.sh >"$XDG_CONFIG_HOME"/bash/git-prompt.sh

create_symlink() {
  local target=$1
  local link_name=$2

  # Check if the symlink already exists
  if [ -e "$link_name" ] || [ -L "$link_name" ]; then
    unlink $2
  fi
  ln -sf "$target" "$link_name"
}


# Symbolic links
create_symlink "$PWD/alacritty.toml" "$XDG_CONFIG_HOME"/alacritty/alacritty.toml
create_symlink "$PWD/.bash_profile" "$HOME"/.bash_profile
create_symlink "$PWD/.bashrc" "$HOME"/.bashrc
create_symlink "$PWD/.zshrc" "$HOME"/.zshrc
create_symlink "$PWD/.env_vars" "$HOME"/.env_vars
create_symlink "$PWD/.shell_aliases" "$HOME"/.shell_aliases
create_symlink "$PWD/.shell_functions" "$HOME"/.shell_functions
create_symlink "$PWD/.shell_post" "$HOME"/.shell_post
create_symlink "$PWD/.inputrc" "$HOME"/.inputrc
create_symlink "$PWD/.tmux.conf" "$HOME"/.tmux.conf
create_symlink "$PWD/nvim" "$XDG_CONFIG_HOME"/nvim

# Second Brain - DO THIS ONLY ON MACOS
# This one's a little tricky because the path contains a space. It needs to be stored as an array,
# and when called it needs to be quoted.
# export SECOND_BRAIN=("$HOME/Library/Mobile Documents/iCloud~md~obsidian/Documents/Mls")
# create_symlink "$SECOND_BRAIN" ~/Mls
