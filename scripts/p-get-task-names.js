#!/usr/bin/env node

const { argv } = require('process');

// Rimuove i primi due argomenti (node e path dello script)
const args = argv.slice(2);
const fullArgs = args.join(' ')
const { project, task } = getProjectAndTaskName(fullArgs, ['/', ':', '|', ','])


const fileName = `projects/${project}/tasks/${cleanForFileName(task)}.md`

console.log(
    JSON.stringify({
        project,
        task,
        title: cleanForTitle(task),
        fileName
    })
)

function getProjectAndTaskName(value, sep) {
    if (sep.length === 0) {
        return {
            project: 'inbox',
            task: value
        }
    }
    let [project, ...rest] = value.split(sep[0])
    if (rest.length) {
        return {
            project,
            task: rest.join(sep)
        }
    } else {
        return getProjectAndTaskName(value, sep.slice(1))
    }
}

function cleanForFileName(value) {
    return value.toLowerCase()
        .replace(/ /g, '-')
        .replace(/\\/g, '.')
        .replace(/\//g, '.')
        .replace(/:/g, '..')
}

function cleanForTitle(inputString) {
    // Rimuove l'eventuale suffisso .md
    let processedString = inputString.replace(/\.md$/, '');

    // Capitalizza tutte le parole a meno che non siano di una lettera sola
    processedString = processedString.split(' ').map(word => {
        // Se la parola è di una sola lettera, la restituisce così com'è
        if (word.length === 1) {
            return word;
        } else {
            // Altrimenti, capitalizza la prima lettera di ogni parola
            return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase();
        }
    }).join(' ');

    return processedString;
}



