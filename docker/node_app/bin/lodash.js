#!/usr/bin/env node


// Importa Lodash
const _ = require('lodash');
const { argv } = require('process');

// Rimuove i primi due argomenti (node e path dello script)
const args = argv.slice(2);

// Il primo argomento è il nome della funzione Lodash
const functionName = args[0];

// Gli argomenti successivi sono passati alla funzione Lodash
const functionArgs = args.slice(1);

// Verifica se la funzione esiste in Lodash
if (typeof _[functionName] === 'function') {
    // Converte gli argomenti se necessario. Questo esempio assume che tutti gli argomenti siano stringhe che necessitano di essere convertite in JSON.
    // Attenzione: questo è un approccio semplificato e potrebbe non essere sicuro o applicabile per tutti i tipi di input.
    const parsedArgs = functionArgs.map(arg => {
        try {
            return JSON.parse(arg);
        } catch (error) {
            return arg; // Restituisce l'argomento originale se il parsing JSON fallisce
        }
    });

    // Invoca la funzione Lodash con gli argomenti e stampa il risultato
    const result = _[functionName](...parsedArgs);
    console.log(result);
} else {
    console.error('Function does not exist in Lodash:', functionName);
}
