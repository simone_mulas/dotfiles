
# Usa un'immagine base Node.js ufficiale
FROM node:20

# Imposta la directory di lavoro all'interno del container
WORKDIR /app

# Copia il file package.json e package-lock.json (se disponibile)
COPY package*.json ./

# Installa le dipendenze del progetto
RUN npm install

# Copia il resto del codice sorgente
COPY . .

# Espone la porta su cui l'applicazione sarà disponibile
EXPOSE 3000

# Comando per avviare l'applicazione
CMD ["node", "index.js"]
