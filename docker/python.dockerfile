
# Usa un'immagine base Python ufficiale
FROM python:3.12

# Imposta la directory di lavoro all'interno del container
WORKDIR /app

# Copia il file requirements.txt
COPY python_app/requirements.txt ./

# Installa le dipendenze del progetto
RUN pip install --no-cache-dir -r requirements.txt

# Copia il resto del codice sorgente
COPY . .

# Espone la porta su cui l'applicazione sarà disponibile
EXPOSE 8000

# Comando per avviare l'applicazione
CMD ["python", "app.py"]
