#!/usr/bin/env python3
import socket
import struct
import sys

def send_wol(mac_address):
    if len(mac_address) == 17 and mac_address.count(':') == 5:
        mac_address = mac_address.replace(':', '')
    elif len(mac_address) != 12:
        raise ValueError("Indirizzo MAC non valido. Deve essere lungo 12 caratteri o 17 con i due punti.")
    
    mac_bytes = bytes.fromhex(mac_address)
    magic_packet = b'\xff' * 6 + mac_bytes * 16
    
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        sock.sendto(magic_packet, ('<broadcast>', 9))

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Uso: script.py <indirizzo MAC>")
        sys.exit(1)
    mac_address = sys.argv[1]
    send_wol(mac_address)
