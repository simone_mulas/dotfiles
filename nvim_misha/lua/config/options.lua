-- Options are automatically loaded before lazy.nvim startup
-- Default options that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/options.lua
-- Add any additional options here

vim.opt.expandtab = true -- Usa spazi al posto di tab
vim.opt.tabstop = 4 -- Numero di spazi che un carattere di tabulazione occupa
vim.opt.shiftwidth = 4 -- Numero di spazi da inserire per un livello di indentazione
vim.opt.softtabstop = 4 -- Fa sì che la pressione di <Tab> e <BS> inserisca o cancelli un numero di spazi pari a softtabstop
vim.opt.swapfile = false
