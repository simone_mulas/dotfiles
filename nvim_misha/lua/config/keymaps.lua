-- Keymaps are automatically loaded on the VeryLazy event
-- Default keymaps that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/keymaps.lua
-- Add any additional keymaps here
--
--
--

vim.api.nvim_set_keymap("n", "<C-A-CR>", ":w<CR>:!echo %<CR>:bd<CR>", { noremap = true, silent = false })
vim.api.nvim_set_keymap("n", "<leader>rn", "<cmd>lua vim.lsp.buf.rename()<CR>", { noremap = true, silent = true })

-- navigate vim panes better
-- vim.keymap.set("n", "<c-k>", ":wincmd k<CR>", { silent = true })
-- vim.keymap.set("n", "<c-j>", ":wincmd j<CR>", { silent = true })
-- vim.keymap.set("n", "<c-h>", ":wincmd h<CR>", { silent = true })
-- vim.keymap.set("n", "<c-l>", ":wincmd l<CR>", { silent = true })
