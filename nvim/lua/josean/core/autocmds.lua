-- Autocmds are automatically loaded on the VeryLazy event
-- Default autocmds that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/autocmds.lua
-- Add any additional autocmds here
--
--
-- -- Definisci un gruppo di autocomandi chiamato MyAutoCmds
local my_auto_cmds_group = vim.api.nvim_create_augroup("MyAutoCmds", {})

vim.api.nvim_create_autocmd("ModeChanged", {
  group = my_auto_cmds_group,
  pattern = "*:n",
  command = "hi Normal guibg=#000000 ctermbg=black",
})

vim.api.nvim_create_autocmd("ModeChanged", {
  group = my_auto_cmds_group,
  pattern = "*:i",
  command = "hi Normal guibg=#022375 ctermbg=black",
})

vim.api.nvim_create_autocmd("ModeChanged", {
  group = my_auto_cmds_group,
  pattern = "*:c",
  command = "hi Normal guibg=#004d13 ctermbg=black",
})

vim.api.nvim_create_autocmd("ModeChanged", {
  group = my_auto_cmds_group,
  pattern = "*:v,*:V",
  command = "hi Normal guibg=#04004d ctermbg=black",
})
