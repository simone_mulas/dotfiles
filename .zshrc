
if [[ "$OSTYPE" == "darwin"* ]]; then
  # needed for brew
  eval "$(/opt/homebrew/bin/brew shellenv)"
else
  eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
fi

# # ~~~~~~~~~~~~~~~ Environment Variables ~~~~~~~~~~~~~~~~~~~~~~~~
if [ -f ~/.env_vars ]; then
    source ~/.env_vars
fi

# # ~~~~~~~~~~~~~~~ Functions ~~~~~~~~~~~~~~~~~~~~~~~~
if [ -f ~/.shell_functions ]; then
    source ~/.shell_functions
fi

# # ~~~~~~~~~~~~~~~ ZSH Config ~~~~~~~~~~~~~~~~~~~~~~~~
export ZSH="$HOME/.oh-my-zsh"
ZSH_THEME="miloshadzic"
DISABLE_AUTO_UPDATE="true"
plugins=(git docker docker-compose)
source $ZSH/oh-my-zsh.sh
# source ~/.m-tools/bin/init.sh
test -e "${HOME}/.iterm2_shell_integration.zsh" && source "${HOME}/.iterm2_shell_integration.zsh" || true

# # ~~~~~~~~~~~~~~~ Aliases ~~~~~~~~~~~~~~~~~~~~~~~~
if [ -f ~/.shell_aliases ]; then
    source ~/.shell_aliases
fi

# # ~~~~~~~~~~~~~~~ Post  ~~~~~~~~~~~~~~~~~~~~~~~~
if [ -f ~/.shell_post ]; then
    source ~/.shell_post
fi


[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh


# ~~~~~~~~~~~~~~~ zoxide ~~~~~~~~~~~~~~~~~~~~~~~~
eval "$(zoxide init zsh)"

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion


